<?php
/**
 * Scarpelli Helloworld
 */

/**
 * class Scarpelli_Helloworld_IndexController
 *
 * Main controller.
 * @author Nicola Scarpelli <info@nicolascarpelli.com>
 * @version 0.1.0
 * @package CMS
 * @license GNU General Public License, version 3
 */
class Scarpelli_Helloworld_IndexController extends Mage_Core_Controller_Front_Action
{
    /**
     * indexAction
     */
    public function indexAction()
    {
       $this->loadLayout();
       $this->renderLayout();
    }
}