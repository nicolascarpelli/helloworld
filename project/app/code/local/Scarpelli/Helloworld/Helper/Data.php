<?php
/**
 * Scarpelli Helloworld
 */

/*
 * class Scarpelli_Helloworld_Helper_Data
 *
 * Main helper.
 * @author Nicola Scarpelli <info@nicolascarpelli.com>
 * @version 0.1.0
 * @package CMS
 * @license GNU General Public License, version 3
 */
class Scarpelli_Helloworld_Helper_Data extends Mage_Core_Helper_Abstract
{
    /**
     * getConfigData
     *
     * Returns the value of the requested configuration.
     * @param string $data
     * @return mixed
     */
    public function getConfigdata($data)
    {
        return Mage::getStoreConfig('scarpelli_helloworld/' . $data);
    }

    /**
     * isEnabled
     *
     * Returns true if the module is enabled to be displayed.
     * @return boolean
     */
    public function isEnabled()
    {
        return $this->getConfigdata('configuration/enabled');
    }

    /**
     * getBaseurl
     *
     * Returns base url of the store.
     * @return string
     */
    public function getBaseurlInHelper()
    {
        return Mage::getBaseUrl();
    }


}