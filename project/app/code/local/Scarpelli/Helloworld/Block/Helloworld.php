<?php
/**
 * Scarpelli Helloworld
 */

/*
 * class Scarpelli_Helloworld_Block_Helloworld
 *
 * Hello World Block.
 * @author Nicola Scarpelli <info@nicolascarpelli.com>
 * @version 0.1.0
 * @package CMS
 * @license GNU General Public License, version 3
 */
class Scarpelli_Helloworld_Block_Helloworld extends Mage_Core_Block_Template
{
    /**isEnabled
     *
     * Returns true if thr module is enabled to be deplayed
     * @return boolean
     */
    public function isEnabled()
    {
        return Mage::helper('scarpelli_helloworld')->isEnabled();
    }

    /**
     * getMessage
     *
     * Returns the custom message
     * @return string
     */

    public function getMessage()
    {
        if ($this->isEnabled()) {
            return Mage::helper('scarpelli_helloworld')->getConfigData('configuration/custom_message');

        }
        return false;
    }

    /**
     * getBaseurl
     *
     * Returns the base url
     * @return string
     */

    public function getBaseurl()
    {
        return Mage::getBaseUrl();
    }

    /**
     * getBaseurl from helper
     *
     * Returns the base url from helper
     *@return string
     */

    public function getBaseurlFromHelper()
    {
        return Mage::helper('scarpelli_helloworld')->getBaseurlInHelper();
    }

}